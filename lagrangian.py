import sympy
from sympy.physics.mechanics import dynamicsymbols, find_dynamicsymbols, init_vprinting
from sympy.matrices import BlockMatrix, Matrix, eye, zeros, ones, diag
from sympy.utilities import lambdify
from sympy import Symbol, symbols, Rational, sin, cos
from sympy import simplify, trigsimp, expand_trig, together, cancel
from typing import Callable, Optional, Union
import numpy
import pickle
from pathlib import Path
import json
import hashlib
from ex_pickler import save_expression, load_expression, generate_hash

t = dynamicsymbols._t


class Lagrangian:
    def __init__(
        self,
        L,
        q: Matrix,
        f_e: Matrix,
        hcstr: list = None,
        nhcstr: list = None,
        **kwargs,
    ):
        # Generalized coordinate
        self.q = q
        self.qDot = q.diff(t)
        self.qDotDot = self.qDot.diff(t)

        # Generate matrices
        ## Constraint matrix

        ### Non holonomic constraints
        self.S = Lagrangian.constraint_matrix(hcstr, nhcstr, self.qDot)
        S = self.S
        SDot = S.diff(t)
        A = S.T
        n, m = A.shape
        self.lambda_vec = Matrix([Symbol(f"lambda_{i+1}") for i in range(m)])

        # Cast L as matrix in order to use jacobian method but it's an expression
        self.lagrangian = L
        L = Matrix([L])
        # d(∂L/∂qdot)/dt - ∂L/∂q
        self.lhs = L.jacobian(self.qDot).diff(t) - L.jacobian(self.q)
        self.rhs = A * self.lambda_vec + f_e

        ## Mass matrix
        self.M = self.lhs.jacobian(self.qDotDot)

        # Solve system augmented with constraints if any
        # We augment vertically the mass matrix with the constraint equations
        # S(q)q' = 0 => S'(q)q' + S(q)q'' = 0 <=> S(q)q'' = -S'(q)q'
        qDotDot_Aug = Matrix([self.qDotDot, self.lambda_vec])  # Augmented coordinates
        self.M_aug = Matrix([[self.M, -A], [S, zeros(m, m)]])
        self.f_aug = Matrix([f_e, -SDot * self.qDot])

        ## Run solve_eom to set those attributes
        self.eom: Matrix = None  # Equation of motion
        self.eom_eval: Matrix = None  # Numerical evaluation
        self.lambda_expr: Matrix = None  # Lambda forces and torque

        # Numerical evaluation
        ## Run generate_numerics to set those attributes
        self.f_n: Callable[Matrix, Matrix, Matrix] = None  # Lambdified qDotDot = F(q,qDot,u)
        self.S_n: Callable[Matrix, Matrix, Matrix] = None  # Lambdidifed constraint equations vector for validation purpose S(q).qDot
        self.lambda_n: Callable[Matrix, Matrix, Matrix] = None  # Lambdified contraint vector

        # File management
        self.pickle_file = Path("eom.mdl")
        self.template_report = Path("template_lagrange_report.md")

        # Generate report
        self.f_e = f_e
        self.name = None

        self.pickle_file = Path("ddr_eom.mdl")

    def solve_required(self) -> bool:
        eom_file_path = self.pickle_file
        if eom_file_path.exists():
            print(f"{eom_file_path} found.")
            eom, eom_hash = load_expression(self.x, eom_file_path)
            if eom_hash == self.hash():
                return False
            else:
                return True
        else:
            return True

    def solve_eom(self, force_solve=False):
        if self.solve_required() or force_solve is True:
            print("Solving Equations Of Motion")
            m, n = self.S.shape
            M_aug_inv = self.M_aug.inv(try_block_diag=True)  # Compute Inverse Augmented Mass Matrix
            rhs_aug = self._simplify_eom(M_aug_inv * self.f_aug)  # Compute augmented rhs and simplify
            self.eom = Matrix(rhs_aug[:-m])
            self.lambda_expr = Matrix(rhs_aug[n:])
            save_expression(self.x, [self.eom, self.lambda_expr], self.pickle_file, self.hash())
        else:
            print(f"EOM loaded from {self.pickle_file}")
            eom, _ = load_expression(self.x, self.pickle_file)
            self.eom = eom[0]
            self.lambda_expr = eom[1]

    # def generate_numerics(self, args: list = [], param_dict: Dict = None, param_file: Path = None):
    def generate_numerics(self, parameters: Union[Path, dict] = None, *args):
        # TODO: Check that eom has been solved
        if isinstance(parameters, dict):
            param = parameters
        elif isinstance(parameters, (Path, str)):
            with open(Path(parameters), "r") as file:
                param = json.load(file)
                # So you can subsitute symbols using string just like {'x':4,'y':2 ...} but it only works on
                # symbols with default assumption, in our case we have symbols with positive=True assomption so we have to use a workaround
                # for instance regenate a dictionnary using symbols as key and with the correct assumptions.
                # NOTE : that we can use generators on dictionnaries too !!!
                # TODO : This solution is kinda hardcoded as positive=True seems a bit too specific. MUST IMPROVE
                param = {Symbol(key, positive=True): value for key, value in param.items()}
        else:  # Assuming that the EOM depends only on q, qDot and inputs
            param = {}

        # twisted but compact way to merge two list [a,b,c] [1,2,3] into [a,1,b,2,c,3]
        states_expr = Matrix([x for pair in zip(self.qDot, self.eom) for x in pair])
        self.f_n = lambdify([self.x, *args], states_expr.subs(param), modules="numpy")
        SqDot = self.S * self.qDot
        self.S_n = lambdify([self.x, *args], SqDot.subs(param), modules="numpy")
        self.lambda_n = lambdify([self.x, *args], self.lambda_expr.subs(param), modules="numpy")

    @staticmethod
    def constraint_matrix(hcstr, nhcstr, qDot: Matrix) -> Matrix:
        S_hcstr = Matrix()
        S_nhcstr = Matrix()

        if hcstr:
            S_hcstr = Matrix(hcstr).diff(t).jacobian(qDot)
        if nhcstr:
            S_nhcstr = Matrix(nhcstr).jacobian(qDot)
        return Matrix([S_hcstr, S_nhcstr])  # S

    def _simplify_eom(self, expr):
        return expr

    @property
    def x(self) -> Matrix:
        """
        Return the X vector, the states of the state-space system XDot = f(X,u).
        If q=[q1,q2,q3 ...] then X = [q1, q1Dot, q2, q2Dot ...]
        :return: a Matrix object.
        """
        X = []
        for q, qDot in zip(self.q, self.qDot):
            X.append(q)
            X.append(qDot)
        return Matrix(X)

    @property
    def xDot(self) -> Matrix:
        """
        Return the XDot vector, the states of the state-space system XDot = f(X,u).
        If q=[q1,q2,q3 ...] then X = [q1, q1Dot, q2, q2Dot ...]
        :return: a Matrix object.
        """
        xDot = []
        for q, qDot in zip(self.qDot, self.qDotDot):
            xDot.append(q)
            xDot.append(qDot)
        return Matrix(xDot)

    # def f(self, states: dict = {}, param: dict = {}) -> Matrix:
    def f(self, x: Matrix, *args) -> Matrix:
        """
        Transition function in a 'standard' form for integration scheme xDot = f(x,u).
        :param x: state vector [q, qDot]
        :return: the state vector xDot [qDot, qDotDot]
        """
        # XDot = []
        # eom = self.eom_eval
        # for qDot, qDotDot in zip(self.qDot, eom):
        # XDot.append(qDot)
        # XDot.append(qDotDot)
        # return Matrix(XDot).subs({**states, **param})
        return self.f_n(q, qDot, *args)

    def eval_constraints_equations(self, q: numpy.array, qDot: numpy.array, param: dict) -> numpy.array:
        if len(q.shape) == 1:
            pass
        elif q.shape[1] == 1:  # If it's a column vector
            pass

        expr = self.S * self.qDot

        return expr.subs(param).tolist()

    def hash(self) -> str:
        # lhs_str = "-".join([str(x) for x in self.lhs])
        # rhs_str = "-".join([str(x) for x in self.rhs])
        # base_str = lhs_str + rhs_str
        # return hashlib.sha256(base_str.encode("utf-8")).hexdigest()
        return generate_hash(self.lhs, self.rhs)

    def generate_report(self):
        from sympy import latex
        from sympy.physics.vector.printing import vpprint, vlatex  # For time derivative

        if self.name:
            name = self.name
        else:
            name = "Lagrange equations"

        with open(self.template_report, "r") as file:
            template_file = file.read()
            tokens = {
                "{Q}": vlatex(self.q, mat_delim="("),
                "{QDOT}": vlatex(self.qDot, mat_delim="("),
                "{LAGRANGIAN}": vlatex(self.lagrangian),
                "{MASS_MATRIX}": vlatex(self.M, mat_delim="("),
                "{CONSTRAINT_MATRIX}": vlatex(self.S, mat_delim="("),
                "{EXTERNAL_FORCES}": vlatex(self.f_e, mat_delim="("),
                "{QDOTDOT}": vlatex(self.qDotDot, mat_delim="("),
                "{EOM}": vlatex(self.eom, mat_delim="("),
                "{LAMBDA_VEC}": vlatex(self.lambda_expr, mat_delim="("),
                "{MODEL_NAME}": name,
                "{LHS}": vlatex(self.lhs.T, mat_delim="("),
                "{RHS}": vlatex(self.rhs, mat_delim="("),
            }
            report_str = template_file
            for token, value in tokens.items():
                report_str = report_str.replace(token, value)
        with open(Path("report.md"), "w") as file:
            file.write(report_str)
