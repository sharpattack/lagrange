from typing import Union, Tuple, List
from sympy import Matrix, symbols, Symbol
from sympy.physics.mechanics import dynamicsymbols
from sympy.core.expr import Expr
from pathlib import Path
import hashlib
import pickle


def save_expression(ds_list: Union[list, Matrix], expr_list: List[Expr], path: Path, expr_hash: str = ""):
    """
    The function `saveExpression` saves a list of expressions along with their corresponding symbols and
    a hash value to a file using pickle serialization.

    :param ds_list: List or sympy matrix  of dynamic symbols to pickle
    :type ds_list: Union[list, Matrix]
    :param expr_list: expr_list is a list of mathematical expressions (Expr objects) that you want to
    save.
    :type expr_list: List[Expr]
    :param path: The `path` parameter is the file path where you want to save the expression data. It
    should be a string representing the file path, including the file name and extension. For example,
    "data/expression_data.pkl"
    :type path: Path
    :param expr_hash: The `expr_hash` parameter is an optional string that represents a hash value for
    the expressions. It can be used to uniquely identify a set of expressions
    :type expr_hash: str
    """
    sym_list = [Symbol(f"x_{i}") for i in range(len(ds_list))]
    relations = [(ds, sym) for (ds, sym) in zip(ds_list, sym_list)]

    with open(Path(path), "wb") as file:
        structure = {
            "symbols": sym_list,
            "expr": [expr.subs(relations) for expr in expr_list],
            "hash": expr_hash,
        }
        pickle.dump(structure, file)


def load_expression(ds_list: Union[list, Matrix], path: Path) -> Tuple[List[Expr], str]:
    """
    The function `load_expression` loads expressions from a file and substitutes symbols with
    corresponding values from a list or matrix.

    :param ds_list: The `ds_list` parameter is a list or sympy matrix of dynamic symbols you want used to pickle the expression
    :type ds_list: Union[list, Matrix]
    :param path: The `path` parameter is the path to the file where the expression structure is stored.
    It should be a string representing the file path
    :type path: Path
    :return: The function `load_expression` returns a tuple containing two elements. The first element
    is a list of `Expr` objects, which are expressions that have been substituted with the given
    `relations`. The second element is a string representing the `hash` value from the loaded structure.
    """
    with open(Path(path), "rb") as file:
        structure = pickle.load(file)
    sym_list = structure["symbols"]
    expr_list = structure["expr"]
    relations = [(sym, ds) for (sym, ds) in zip(sym_list, ds_list)]
    return ([expr.subs(relations) for expr in expr_list], structure["hash"])


def generate_hash(ds_list: Union[list, Matrix], expr_list: Union[list, Matrix]) -> str:
    symbols_str = "-".join([str(ds) for ds in ds_list])
    expr_str = "-".join([str(ds) for ds in expr_list])
    base_str = symbols_str + expr_str

    return hashlib.sha256(base_str.encode("utf-8")).hexdigest()


if __name__ == "__main__":
    x, y, z = symbols("x y z")
    theta = dynamicsymbols("theta")
    thetaDot = theta.diff()

    expr = Matrix([x * theta + y * z * thetaDot, y**2 * thetaDot])
    ds_list = Matrix([theta, thetaDot])
    file = Path("ex_pickler_test.mdl")
    expr_hash = generate_hash(ds_list, expr)
    save_expression(ds_list, [expr, x * y * theta], file, expr_hash)
    print(load_expression(ds_list, file))
