from lagrangian import Lagrangian
import sympy
from sympy.physics.mechanics import dynamicsymbols, find_dynamicsymbols, init_vprinting
from sympy.matrices import BlockMatrix, Matrix, eye, zeros, ones, diag
from sympy.utilities import lambdify
from sympy import Symbol, symbols, Rational, sin, cos
from sympy import simplify, trigsimp, expand_trig, together, cancel
from typing import Callable, Optional, Union
import numpy
import pickle
from pathlib import Path
import json
import hashlib
from ex_pickler import save_expression, load_expression, generate_hash


class DDR(Lagrangian):
    def __init__(self, param_file: Path):
        ## Coordinates
        q = Matrix(dynamicsymbols("x y theta phi_2 phi_3"))
        x, y, theta, phi_2, phi_3 = q.T.tolist()[0]
        xDot, yDot, thetaDot, phi_2Dot, phi_3Dot = q.diff().T.tolist()[0]

        ## Inputs
        T_2, T_3 = symbols("T_2 T_3")
        self.inputs = [T_2, T_3]

        ## Parameters
        m, J, L_w, D, m_w, Jw_zz, Jw_xx, alpha_t, alpha_r, rho = symbols("m J L D m_w Jw_zz Jw_xx alpha_t alpha_r rho", positive=True)
        with open(param_file, "r") as file:
            p = json.load(file)

        self.param = {
            m: p["m"],
            J: p["J"],
            L_w: p["L"],
            alpha_r: p["alpha_r"],
            alpha_t: p["alpha_t"],
            D: p["D"],
            rho: p["rho"],
        }

        ## Constraints
        nhcstr = [
            xDot - D / 4 * sin(theta) * (phi_2Dot + phi_3Dot),
            yDot + D / 4 * cos(theta) * (phi_2Dot + phi_3Dot),
            thetaDot + D / (2 * L_w) * (phi_2Dot - phi_3Dot),
        ]

        ## External forces
        f_e = Matrix(
            [
                -alpha_t * xDot,
                -alpha_t * yDot,
                -alpha_r * thetaDot,
                T_2 - rho * phi_2Dot,
                T_3 - rho * phi_3Dot,
            ]
        )

        ## Lagrangian
        L = m * (xDot**2 + yDot**2) / 2 + J * (thetaDot**2) / 2
        super().__init__(L, q, f_e, nhcstr=nhcstr)

        ## Solve augmented inverse matrix
        self.pickle_file = Path("ddr_eom.mdl")

        self.solve_eom()
        self.generate_numerics(param_file, T_2, T_3)
        ## Numerical
        # self.preEval(self.param)  # Replace parameters by their numerical values

    def f(self, states: list = [], inputs: list = []) -> Matrix:
        state_dict = {}
        input_dict = {}
        for symbol, value in zip(self.x, states):
            state_dict[symbol] = value
        for symbol, value in zip(self.inputs, inputs):
            input_dict[symbol] = value
            # f(q,qDot,*args)
        return super().f(state_dict, input_dict)

    def _simplify_eom(self, expr):
        return cancel(expand_trig(simplify(expr)))  # Compute augmented rhs and simplify


if __name__ == "__main__":
    from sympy.printing.pretty.pretty import pretty_print as pprint

    sympy.init_printing()
    param_path = Path("ddr_param.json")
    ddr = DDR(param_path)

    # ddr = DDR()
    pprint(ddr.q)
    pprint(ddr.qDot)
    pprint(ddr.rhs)
    pprint(ddr.lhs)
    pprint(ddr.M)
    pprint(ddr.f_aug)
    pprint(ddr.M_aug)
    eom = ddr.eom
    pprint(eom)
    ddr.generate_report()

    # Simulation
    if True:
        import numpy as np
        from scipy.integrate import odeint
        import matplotlib.pyplot as plt

        # x xDot y yDot theta thetaDot phi2 phi2Dot phi3 phi3Dot
        q0 = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

        fs = 2000  # Sampling frequency [Hz]
        ts = 1 / fs  # Samling time [s]
        tf = 10  # Simulation time [s]
        Ns = int(fs * tf)  # Number of sample
        t = np.linspace(0, tf, Ns)
        q = np.zeros((len(t), len(q0)))

        def dynamics(x, t, T_2_n, T_3_n):
            if t < 2:
                T_2 = 0.2 * numpy.sin(2 * np.pi / 10 * t)
                T_3 = 0.1
            else:
                T_2 = 0
                T_3 = 0
            f = ddr.f_n(x, T_2, T_3).transpose()[0]
            return f

        # Integration
        q = odeint(dynamics, q0, t, args=(0.0, 0.0))

        ## Plot
        fig = plt.figure()
        plt.plot(t, q[:, [4, 5]])
        fig.legend(["theta", "thetaDot"])
        plt.grid(True)
        plt.xlabel("$t [s]$")
        plt.ylabel(r"$\theta [s]$")
        plt.savefig("theta_thetaDot.svg")

        fig = plt.figure()
        plt.plot(q[:, [0]], q[:, [2]])
        plt.axis("equal")
        fig.legend(["trajectoire"])
        plt.grid(True)
        plt.xlabel(r"$x [m]$")
        plt.ylabel(r"$y [m]$")
        plt.savefig("x_y.svg")

        fig = plt.figure()
        cstr = np.array([ddr.S_n(q[i], 0, 0) for i in range(len(t))])
        plt.plot(t, cstr[:, [0]].reshape((Ns,)))
        plt.plot(t, cstr[:, [1]].reshape((Ns,)))
        plt.plot(t, cstr[:, [2]].reshape((Ns,)))
        fig.legend(["$S(q)\dot{q}^1$", "$S(q)\dot{q}^2$", "$S(q)\dot{q}^3$"])
        plt.grid(True)
        plt.xlabel("$t [s]$")
        plt.ylabel("$\epsilon$")
        plt.savefig("error.svg")

        plt.show()
